/* eslint consistent-return:0 */
import path, {resolve} from 'path';
import url from 'url';
import proxy from 'express-http-proxy';
import express from 'express';
import logger from './logger';
import configs from '../configs';
import argv from './argv';
import port from './port';
import setup from './middlewares/frontendMiddleware';
import cookieParser from 'cookie-parser';
import HTTPProxy from 'http-proxy';

const app = express();

app.use(cookieParser());

app.use('/api', proxy(configs.APIPROXY, {
    // eslint-disable-next-line
    forwardPath: (req, res) => {
        console.log(url.parse(req.url).path);
        return url.parse(req.url).path;
    },
    limit: '1024mb'
}));


app.use('/public', express.static(path.join(__dirname, '../', 'public')));
setup(app, {
    outputPath: resolve(process.cwd(), 'build'),
    publicPath: '/',
});


// get the intended host and port number, use localhost and port 3000 if not provided
const customHost = argv.host || process.env.HOST;
const host = customHost || null; // Let http.Server use its default IPv6/4 host
const prettyHost = customHost || 'localhost';


//Proxy WS
HTTPProxy.createServer({
    target: 'ws://171.244.4.116:8065',
    ws: true,
}).listen(8014, (err) => {
    console.log(err);
    logger.appStarted(8014, prettyHost);
});

app.listen(port, host, (err) => {
    if (err) {
        return logger.error(err.message);
    }
    logger.appStarted(port, prettyHost);
});
