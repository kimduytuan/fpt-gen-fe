/* eslint-disable global-require */

/**
 * Front-end middleware
 */
import addDevMiddlewares from './addDevMiddlewares';
import addProdMiddlewares from './addProdMiddlewares';
import configs from '../../configs'

const setUp = (app, options) => {
    const isProd = process.env.NODE_ENV === 'prod';
    const mainJs = isProd ? configs.main : configs.mainDev;
    app.set('view engine', 'ejs');
    if (isProd) {
        addProdMiddlewares(app, options);
    } else {
        const webpackConfig = require('../../configs/webpack/webpack.dev.babel');
        addDevMiddlewares(app, webpackConfig);
    }

    app.get('*', (req, res) => {
        var links = [
            {href: 'http://recruit.framgia.vn/', text: 'Framgia Việt Nam Tuyển Dụng'},
            {href: 'https://www.facebook.com/FramgiaVietnam/', text: 'Framgia Việt Nam Facebook'},
            {href: 'https://viblo.asia/', text: 'Viblo by Framgia'},
            {href: '/', text: 'Text Link 1'},
            {href: '/', text: 'Text Link 2'},
            {href: '/', text: 'Text Link 3'},
            {href: '/', text: 'Text Link 4'},
        ];
        let dataServerSide = links;
        var headline = 'Framgia Viet Nam';
        var tagline = "IT là lĩnh vực công bình và không giới hạn, nơi mỗi cá nhân được chia sẻ cơ hội và nhìn nhận thông qua nỗ lực thực sự. Tận dụng những lợi thế của IT mang lại, chúng tôi không ngừng hoàn thiện, trở thành nền tảng cho sự phát triển dịch vụ toàn cầu.";
        res.render('index', {
            links: links,
            headline: headline,
            tagline: tagline,
            mainJs,
            dataServerSide
        });
    });
    return app;
};

export default setUp;
