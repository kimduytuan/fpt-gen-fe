import {fromJS} from "immutable";

function languageProviderReducer(state = fromJS({}), action) {
    switch (action.type) {
        case 'Task':
            return state
                .set('locale', action.locale);
        default:
            return state;
    }
}


export default {
    languageProviderReducer
}
