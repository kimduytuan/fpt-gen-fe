import React, {Component} from 'react';
require('offline-plugin/runtime').install();
import {Switch, Route} from 'react-router-dom';
import {ProgressBar} from 'react-fetch-progressbar';

import Home from './containers/Home/Loadable'

class App extends Component {
    render() {
        return (
            <div>
                <ProgressBar style={{
                    position: 'absolute',
                    top: '0',
                    zIndex: '9000',
                    backgroundColor: '#3074c1',
                    height: '6px',
                }}/>
                <Switch>
                    <Route exact path="/" component={Home}/>
                </Switch>
            </div>
        );
    }
}

export default App;
