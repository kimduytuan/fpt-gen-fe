/**
 * Create the store with dynamic reducers
 */

import {createStore, applyMiddleware, compose} from 'redux';
import {fromJS} from 'immutable';
import {routerMiddleware} from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import createReducer from './rootReducers.js';
import RootSaga from './rootSagas'

const sagaMiddleware = createSagaMiddleware();

function logRedux({getState}) {
    return next => action => {
        console.info(action);
        console.info('state after dispatch', getState());
        return next(action);
    }
}

export default function configureStore(initialState = {}, history) {
    const middlewares = [
        sagaMiddleware,
        routerMiddleware(history),
        logRedux
    ];

    const enhancers = [
        applyMiddleware(...middlewares),
    ];

    const store = createStore(
        createReducer(),
        fromJS(initialState),
        compose(...enhancers)
    );

    // Extensions
    sagaMiddleware.run(RootSaga);
    return store;
}
