import React, {PropTypes} from 'react';
import './style.css';
import {connect} from 'react-redux';

export class Home extends React.Component {

    constructor(props) {
        super(props);
        this.path = this.props.location.pathname;
    }

    componentWillMount() {
        console.log((window.DataServerSide));
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <p className="App-intro">
                    To get started, edit <code>src/App.js</code> and save to reload.
                </p>
            </div>
        );
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        dispatch
    };
}


function StateToProps(store) {
    return {
        state: store,
    };
}


export default connect(StateToProps, mapDispatchToProps)(Home);

