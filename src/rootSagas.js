import {call, put, select, takeLatest, all, take, race} from 'redux-saga/effects';
import {delay} from 'redux-saga';
import HomeSaga from './containers/Home/saga';

export function* getChannels(actions) {
    // Select username from store
    console.log(actions);

}


export function* watchGetChannels() {
    while (true) {
        const data = yield take('TASK_CHANNELS');
        yield put({
            type: 'CANCEL_TASK_CHANNELS',
        });
        yield race({
            task: call(getChannels, data),
            cancel: take('CANCEL_TASK_CHANNELS'),
            timeout: call(delay, 1000),
        });
    }
}


export default function* root() {
    yield all([
        watchGetChannels(),
        HomeSaga()
    ]);
}
